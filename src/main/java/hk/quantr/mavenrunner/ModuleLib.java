package hk.quantr.mavenrunner;

import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

/**
 *
 * @author peter
 */
public class ModuleLib {

	public static boolean isDebug = true;

	public static void log(String str) {
		if (isDebug) {
			InputOutput io = IOProvider.getDefault().getIO("Maven Runner", false);
			io.getOut().println(str);
		}
	}

	public static void log(Object obj) {
		log(obj.toString());
	}
}
