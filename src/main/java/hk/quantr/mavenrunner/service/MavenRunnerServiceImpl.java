//package hk.quantr.mavenrunner.service;
//
//import hk.quantr.mavenrunner.MavenRunnerTopComponent;
//import hk.quantr.mavenrunner.MyTreeNode;
//import javax.swing.JTree;
//import javax.swing.tree.DefaultMutableTreeNode;
//import javax.swing.tree.DefaultTreeModel;
//import javax.swing.tree.TreePath;
//import org.netbeans.api.project.ProjectInformation;
//import org.openide.util.lookup.ServiceProvider;
//
///**
// *
// * @author peter
// */
//@ServiceProvider(service = MavenRunnerService.class)
//public class MavenRunnerServiceImpl extends MavenRunnerService {
//
//	@Override
//	public boolean run(String projectName, String goal) {
//		MavenRunnerTopComponent mavenRunnerTopComponent = (MavenRunnerTopComponent) MavenRunnerTopComponent.getRegistry().getActivated();
//
//		JTree projectTree = mavenRunnerTopComponent.projectTree;
//		DefaultTreeModel model = (DefaultTreeModel) projectTree.getModel();
//		DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
//		for (int i = 0; i < root.getChildCount(); i++) {
//			MyTreeNode projectNode = (MyTreeNode) root.getChildAt(i);
//			ProjectInformation pi = projectNode.projectInformation;
//			if (pi.getDisplayName().equals(projectName)) {
//				for (int x = 0; x < projectNode.getChildCount(); x++) {
//					MyTreeNode goalNode = (MyTreeNode) projectNode.getChildAt(x);
//					if (goalNode.name.equals(goal)) {
//						mavenRunnerTopComponent.runGoal(goalNode);
//						break;
//					}
//				}
//			}
//		}
//		return true;
//	}
//
//	@Override
//	public void selectNode(String projectName, String goal) {
//		MavenRunnerTopComponent mavenRunnerTopComponent = (MavenRunnerTopComponent) MavenRunnerTopComponent.getRegistry().getActivated();
//
//		JTree projectTree = mavenRunnerTopComponent.projectTree;
//		DefaultTreeModel model = (DefaultTreeModel) projectTree.getModel();
//		DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
//		for (int i = 0; i < root.getChildCount(); i++) {
//			MyTreeNode projectNode = (MyTreeNode) root.getChildAt(i);
//			ProjectInformation pi = projectNode.projectInformation;
//			if (pi.getDisplayName().equals(projectName)) {
//				for (int x = 0; x < projectNode.getChildCount(); x++) {
//					MyTreeNode goalNode = (MyTreeNode) projectNode.getChildAt(x);
//					if (goalNode.name.equals(goal)) {
//						projectTree.setSelectionPath(new TreePath(model.getPathToRoot(goalNode)));
//						projectTree.scrollPathToVisible(new TreePath(model.getPathToRoot(goalNode)));
//					}
//				}
//			}
//		}
//	}
//
//}
